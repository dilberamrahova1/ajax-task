module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: "standard",
  overrides: [
    {
      env: {
        node: true,
      },
      files: [".eslintrc.{js,cjs}"],
      parserOptions: {
        sourceType: "script",
      },
    },
  ],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    eqeqeq: "off",
    curly: "error",
    quotes: ["error", "double"],
    semi: "off",
    "no-unused-vars": "off",
    "prefer-const": "off",
    "space-before-function-paren": "off",
    "lines-between-class-members": "off",
    "import/first": "off",
    "comma-dangle": ["error", "always-multiline"],
    "no-console": "off",
    " appearance": "none",
    "-webkit-appearance": "none",
  },
};
