const VALID_EMAIL_ENDINGS = ["gmail.com", "outlook.com", "yandex.ru"];

function validate(input) {
  const domain = input.toString().split("@")[1];
  return VALID_EMAIL_ENDINGS.includes(domain);
}
export default validate;
