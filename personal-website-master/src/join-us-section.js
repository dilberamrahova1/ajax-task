class SectionCreator {
  create(type) {
    if (type === "standart") {
      return this.createStandart();
    } else if (type === "advanced") {
      return this.createadvanced();
    }
  }
  createStandart() {
    // DOM AND DOM API
    let task = document.getElementById("task");

    let photo = document.createElement("img");
    photo.className = "photo";
    let title = document.createElement("h1");
    title.className = "title";
    let subtitle = document.createElement("h2");
    subtitle.className = "subtitle";
    let input = document.createElement("input");
    input.className = "input";
    let button = document.createElement("button");
    button.className = "button";

    input.setAttribute("type", "email");
    input.setAttribute("placeholder", "email");
    photo.setAttribute("src", "assets/images/photo.png");
    photo.setAttribute("alt", "");
    button.innerHTML = "SUBSCRIBE";

    title.textContent = "Join Our Program";
    subtitle.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

    task.appendChild(photo);
    task.appendChild(title);
    task.appendChild(subtitle);
    task.appendChild(input);
    task.appendChild(button);

    // CSS
    task.style.position = "relative";
    title.style.position = "absolute";
    title.style.color = "white";
    title.style.left = "50%";
    title.style.top = "50%";
    title.style.transform = " translate(-50%, -50%)";
    title.style.height = "392px";
    title.style.width = "480px";
    title.style.fontFamily = "Oswald";
    title.style.fontSize = "48px";
    title.style.lineHeight = "64px";
    title.style.textAlign = "center";

    subtitle.style.position = "absolute";
    subtitle.style.color = "rgba(255, 255, 255, 0.7)";
    subtitle.style.left = "50%";
    subtitle.style.top = "50%";
    subtitle.style.transform = " translate(-50%, -50%)";
    subtitle.style.height = "215px";
    subtitle.style.width = "452px";
    subtitle.style.fontFamily = "Source Sans Pro";
    subtitle.style.fontSize = "24px";
    subtitle.style.lineHeight = "32px";
    subtitle.style.textAlign = "center";
    input.style.position = "absolute";
    input.style.left = "40%";
    input.style.top = "50%";
    input.style.transform = " translate(-50%, -50%)";
    input.style.width = "462px";
    input.style.height = "34px";
    input.style.opacity = "0.2";
    button.style.backgroundColor = "rgba(85, 194, 216, 1)";
    button.style.border = "2px solid rgba(85, 194, 216, 1)";
    button.style.color = "white";
    button.style.width = "111px";
    button.style.height = "36px";
    button.style.borderRadius = "18px";
    button.style.position = "absolute";
    button.style.left = "62%";
    button.style.top = "46%";
    button.style.cursor = "pointer";

    // Events
    // const enterInput = function () {
    //   if (input.value) {
    //     alert("Hello!");
    //   } else {
    //     alert("Error!");
    //   }
    // };

    // button.addEventListener("click", enterInput);
  }
  createadvanced() {
    // DOM AND DOM API
    let task1 = document.getElementById("task1");

    let photo = document.createElement("img");
    photo.className = "photo";
    let title = document.createElement("h1");
    title.className = "title";
    let subtitle = document.createElement("h2");
    subtitle.className = "subtitle";
    let input = document.createElement("input");
    input.className = "input";

    let button = document.createElement("button");
    button.className = "button";

    input.setAttribute("type", "text");
    input.setAttribute("placeholder", "email");
    photo.setAttribute("src", "assets/images/photo.png");
    photo.setAttribute("alt", "");
    button.innerHTML = "Subscribe to Advanced Program.";
    title.textContent = "Join Our Advanced Program";
    subtitle.textContent =
      "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

    task1.appendChild(photo);
    task1.appendChild(title);
    task1.appendChild(subtitle);
    task1.appendChild(input);
    task1.appendChild(button);

    // CSS
    task1.style.position = "relative";
    title.style.position = "absolute";
    title.style.color = "white";
    title.style.left = "50%";
    title.style.top = "50%";
    title.style.transform = " translate(-50%, -50%)";
    title.style.height = "392px";
    title.style.width = "480px";
    title.style.fontFamily = "Oswald";
    title.style.fontSize = "48px";
    title.style.lineHeight = "64px";
    title.style.textAlign = "center";

    subtitle.style.position = "absolute";
    subtitle.style.color = "rgba(255, 255, 255, 0.7)";
    subtitle.style.left = "50%";
    subtitle.style.top = "60%";
    subtitle.style.transform = " translate(-50%, -50%)";
    subtitle.style.height = "215px";
    subtitle.style.width = "452px";
    subtitle.style.fontFamily = "Source Sans Pro";
    subtitle.style.fontSize = "24px";
    subtitle.style.lineHeight = "32px";
    subtitle.style.textAlign = "center";
    input.style.position = "absolute";
    input.style.left = "40%";
    input.style.top = "65%";
    input.style.transform = " translate(-50%, -50%)";
    input.style.width = "462px";
    input.style.height = "34px";
    input.style.opacity = "0.2";
    button.style.backgroundColor = "rgba(85, 194, 216, 1)";
    button.style.border = "2px solid rgba(85, 194, 216, 1)";
    button.style.color = "white";
    button.style.width = "212px";
    button.style.height = "36px";
    button.style.borderRadius = "18px";
    button.style.position = "absolute";
    button.style.left = "62%";
    button.style.top = "61%";
    button.style.cursor = "pointer";

    // Events
    // const enterInput = function () {
    //   if (input.value) {
    //     alert("Hello!");
    //   } else {
    //     alert("Error!");
    //   }
    // };

    // button.addEventListener("click", enterInput);
  }
}

export default SectionCreator;
