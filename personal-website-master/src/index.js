/* eslint-disable  */
import "./styles/style.css";
import SectionCreator from "./join-us-section.js";
const sectionCreator = new SectionCreator();

const standartProgram = sectionCreator.create("standart");
const advancedProgram = sectionCreator.create("advanced");

setTimeout(function () {
  let task1 = document.getElementById("task1");
  if (task1) {
    task1.remove();
  }
}, 10000);

import validate from "./email-validator.js";
// subscribe and unsubscirbe
let subscribed = false;
const input = document.querySelector(".input");
const button = document.querySelector(".button");

input.addEventListener("input", function (event) {
  const emailValue = event.target.value;
  localStorage.setItem("email", emailValue);
});

function checkEmail(email) {
  const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return regex.test(email);
}

button.addEventListener("click", function () {
  const inputValue = input.value;
  const result = checkEmail(inputValue);

  if (result) {
    button.disabled = true;
    button.style.opacity = "0.5";

    // Sending the email to the server
    sendSubscribeRequest(inputValue);
  }
});

function sendSubscribeRequest(email) {
  const endpoint = "http://localhost:8080/subscribe";

  fetch(endpoint, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email }),
  })
    .then((response) => {
      if (response.status === 200) {
        return response.json();
      } else if (response.status === 422) {
        return response.json().then((data) => {
          alert(data.error);
        });
      } else {
        throw new Error("Server error");
      }
    })
    .then((data) => {
      hideInput(email, true);
    })
    .catch((error) => {
      console.error("Error:", error);
    })
    .finally(() => {
      button.disabled = false;
      button.style.opacity = "1";
    });
}

function hideInput(email, valid) {
  if (valid && !subscribed) {
    input.style.display = "none";
    button.textContent = "Unsubscribe";
    button.style.left = "50%";
    button.style.top = "50%";
    button.style.transform = "translate(-50%, -50%)";
    subscribed = true;
    localStorage.setItem("subscribed", subscribed);
  } else if (valid && subscribed) {
    input.style.display = "block";
    button.textContent = "Subscribe";
    button.style.position = "absolute";
    button.style.left = "65%";
    subscribed = false;
    localStorage.setItem("subscribed", subscribed);
  } else {
    input.style.display = "block";
    button.textContent = "Subscribe";
  }
}

window.addEventListener("load", function () {
  const emailValue = localStorage.getItem("email");

  if (emailValue) {
    input.value = emailValue;
  }

  const subscribedValue = localStorage.getItem("subscribed");
  if (subscribedValue) {
    subscribed = subscribedValue === "true";
    hideInput(emailValue, subscribed);
  }
});

// community
const communitySection = document.getElementById("community-section");

function getCommunity() {
  const xhr = new XMLHttpRequest();

  xhr.open("GET", "http://localhost:8080/community");

  xhr.onload = function () {
    if (xhr.status === 200) {
      const communityData = JSON.parse(xhr.responseText);

      const communityElements = communityData.map((member) => {
        const memberDiv = document.createElement("div");

        const memberImg = document.createElement("img");
        memberImg.src = member.avatar;

        const memberName = document.createElement("h3");
        memberName.textContent = `${member.firstName} ${member.lastName}`;

        const memberPosition = document.createElement("p");
        memberPosition.textContent = member.position;

        memberDiv.appendChild(memberImg);
        memberDiv.appendChild(memberName);
        memberDiv.appendChild(memberPosition);

        return memberDiv;
      });

      for (let element of communityElements) {
        communitySection.appendChild(element);
      }
    } else {
      console.error(xhr.statusText);
    }
  };

  xhr.send();
}

window.addEventListener("load", getCommunity);
